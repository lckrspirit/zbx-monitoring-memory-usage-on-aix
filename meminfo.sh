#!/bin/bash 
 
totalmem=`lsattr -El sys0 -a realmem | awk {'print $2'}` 
usedmem=`svmon -G -O unit=KB | head -4 | tail -1 | awk {'print $3'}` 
freemem=`expr $totalmem - $usedmem` 
 
case $1 in 
    "--free") 
        echo "$freemem" 
        ;; 
 
    "--used") 
        echo "$usedmem" 
        ;; 
 
    "--total") 
        echo "$totalmem" 
        ;; 
 
    *) 
        echo "Flags:" 
        echo " --free  - Free mem" 
        echo " --used  - Used mem" 
        echo " --total - Total mem" 
 
esac